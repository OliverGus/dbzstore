﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDatabase db = new PedidoDatabase();
            int idpedido = db.Salvar(pedido);

            PedidoItemBusiness itembusiness = new PedidoItemBusiness();
            foreach(ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemdto = new PedidoItemDTO();
                itemdto.IdPedido = idpedido;
                itemdto.IdProduto = item.Id;

                itembusiness.Salvar(itemdto);
            }

            return idpedido;

        }
        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }
    }
}
