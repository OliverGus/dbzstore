﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using Nsf._2018.Modulo3.App.DB.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO produto )
        {
            string script =
            @"INSERT INTO tb_produto(nm_produto, vl_preco)  
				            VALUES (@nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto",produto.Id ));
            parms.Add(new MySqlParameter("nm_produto",produto.Produto ));
            parms.Add(new MySqlParameter("vl_preco", produto.preço));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public List<ProdutoDTO> Consultar(string Nome)
        {
            Database db = new Database();
            string script =
                @"SELECT * FROM tb_produto
                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + Nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO novodto = new ProdutoDTO();
                novodto.Id = reader.GetInt32("id_produto");
                novodto.Produto = reader.GetString("nm_produto");
                novodto.preço = reader.GetInt32("vl_preco");
                lista.Add(novodto);
            }
            reader.Close();
            return lista;
        }
        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.preço = reader.GetInt32("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
