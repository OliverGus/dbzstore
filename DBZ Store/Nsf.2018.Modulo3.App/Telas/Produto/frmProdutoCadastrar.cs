﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO DTO = new ProdutoDTO();
                DTO.Produto = txtProduto.Text;
                DTO.preço = Convert.ToInt32(txtPreco.Text);

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(DTO);

                MessageBox.Show("salvo com sucesso");
            }
            
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao salvar a música: " + ex.Message);
            }
            this.Hide();
            
        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
